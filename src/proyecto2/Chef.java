/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2;

import java.io.File;
import java.util.*;
import static proyecto2.Menu.cocinero;
import static proyecto2.Menu.mesero;

/**
 *
 * @author Ximena Puchaicela
 */
public class Chef extends Personal {

    public int hours;
    private final int valorSalary = 4;
    Menu menu1 = new Menu();

    public Chef() {
    }

    public Chef(int horas, String nombre, String apellido, int edad, int cedula) {
        super(nombre, apellido, edad, cedula);
        this.hours = horas;
    }

    @Override
    public double salary() {
        salary = hours * valorSalary;
        return salary;
    }

    public void select() {
        int option, category, counter = 0;
        boolean valor = true;
        Scanner teclado = new Scanner(System.in);
//        File ArchivoTXT = new File("C.Ecuatoriana.txt");
//        File ArchivoTXT1 = new File("C.Internacional.txt");
//        File ArchivoTXT2 = new File("Pasteleria.txt");
//        File ArchivoTXT3 = new File("Cocteles.txt");
        do {
            System.out.println("\n1.Agregar Receta\n2.Armar Menu\n3.Salir");
            option = teclado.nextInt();
            valor = true;
            if (option == 3) {
                valor = false;
            }
            while (valor) {
                switch (option) {
                    case 1:
                        System.out.println("Elija la categoria a la cual desea agregar una receta\n1.Cocina Ecuatoriana\n2.Cocina Internacional\n3.Postres\n4.Cocteleria\n5.Menu Principal");
                        category = teclado.nextInt();
                        switch (category) {
                            case 1:
                                Menu.file(ArchivoTXT);
                                Menu llamada = new Menu();
                                llamada.add_recipe(ArchivoTXT);
                                break;
                            case 2:
                                Menu.file(ArchivoTXT1);
                                Menu receta = new Menu();
                                receta.add_recipe(ArchivoTXT1);
                                break;
                            case 3:
                                Menu.file(ArchivoTXT2);
                                Menu postre = new Menu();
                                postre.add_recipe(ArchivoTXT2);
                                break;
                            case 4:
                                Menu.file(ArchivoTXT3);
                                Menu coctel = new Menu();
                                coctel.add_recipe(ArchivoTXT3);
                                break;
                            case 5:
                                valor = false;
                                break;
                        }
                        break;
                    case 2:
                        System.out.println("Elija la categoria a la cual desea buscar una receta\n1.Cocina Ecuatoriana\n2.Cocina Internacional\n3.Postres\n4.Cocteleria\n5.Atras");
                        category = teclado.nextInt();
                        switch (category) {
                            case 1:
                                System.out.println("1.Desayuno\n2.Almuerzo\n3.Merienda\n4.Atras");
                                counter = teclado.nextInt();
                                if (counter == 2 || counter == 3) {
                                    cocinero.clear();
                                    mesero.clear();
                                }
                                Menu.verify(ArchivoTXT);
                                Menu llamada = new Menu();
                                llamada.search(ArchivoTXT);
                                break;
                            case 2:
                                System.out.println("1.Desayuno\n2.Almuerzo\n3.Merienda\n4.Atras");
                                counter = teclado.nextInt();
                                if (counter == 2 || counter == 3) {
                                    cocinero.clear();
                                    mesero.clear();
                                }
                                Menu.verify(ArchivoTXT1);
                                Menu receta = new Menu();
                                receta.search(ArchivoTXT1);
                                break;
                            case 3:
                                System.out.println("1.Desayuno\n2.Almuerzo\n3.Merienda\n4.Atras");
                                counter = teclado.nextInt();
                                if (counter == 2 || counter == 3) {
                                    cocinero.clear();
                                    mesero.clear();
                                }
                                Menu.verify(ArchivoTXT2);
                                Menu postre = new Menu();
                                postre.search(ArchivoTXT2);
                                break;
                            case 4:
                                System.out.println("1.Desayuno\n2.Almuerzo\n3.Merienda\n4.Atras");
                                counter = teclado.nextInt();
                                if (counter == 2 || counter == 3) {
                                    cocinero.clear();
                                    mesero.clear();
                                }
                                Menu.verify(ArchivoTXT3);
                                Menu coctel = new Menu();
                                coctel.search(ArchivoTXT3);
                                break;
                            case 5:
                                valor = false;
                                break;

                        }

                }
            }

        } while (option == 1 || option == 2);

    }

    @Override
    public String toString() {
        salary();
        return super.toString() + "sueldo: " + salary;
    }
}
