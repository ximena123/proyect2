/*

 */
package proyecto2;

import java.util.Random;

/**
 *
 * @author Ximena Puchaicela
 */
// inicializa todos los valores de los atributos de receta//
public class Recipe {

    String code;
    String name;
    String ingredients;
    String process;
    int price;
    String separator = "/";
    int record_size;

    public Recipe() {
    }

    ;
    public Recipe(String name, String ingredients, String process, int price) {
        this.name = name;
        this.ingredients = ingredients;
        this.process = process;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getProcess() {
        return process;
    }

    public String getCode() {
        char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
        int charsLength = chars.length;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < 5; i++)
            buffer.append(chars[random.nextInt(charsLength)]);
        code=  buffer.toString();
        return code;
    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }

    @Override
    public String toString() {
        return "Recipe{" + "code=" + code + ", name=" + name + ", ingredients=" + ingredients + ", process=" + process + ", price=" + price + ", separator=" + separator + ", record_size=" + record_size + '}';
    }

    public int getSize() {
        record_size = getCode().length() + separator.length() + getName().length() + separator.length() + getIngredients().length() + separator.length() + getProcess().length() + separator.length() + getPrice() + separator.length();
        return record_size;
    }
}
