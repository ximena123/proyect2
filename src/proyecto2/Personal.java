/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2;

/**
 *
 * @author Ximena Puchaicela
 */
class Personal extends Restaurant {
    String name;
    String lastName;
    int age;
    int id;
    public Personal(){}
    public Personal(String name, String lastName, int age, int id) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int edad) {
            this.age=edad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
   @Override
   public double salary(){
        return salary;
   }
    @Override
    public String toString() {
        return "Personal{" + "nombre=" + name + ", apellido=" + lastName + ", edad=" + age + ", cedula=" + id + '}';
    }
    
}
