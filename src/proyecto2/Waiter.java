/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2;
import static proyecto2.Menu.mesero;

/**
 *
 * @author Ximena Puchaicela
 */
class Waiter extends Chef {
    final double basico=375;
    double propinas;
    String pedidos[]= new String[200];
    public Waiter(){}
    public Waiter(int horas, String nombre, String apellido, int edad, int cedula) {
        super(horas, nombre, apellido, edad, cedula);
    }
    public double getPropinas() {
        return propinas;
    }
    public void setPropinas(double propinas) {
        this.propinas = propinas;
    }
    public double salary(){
        salary = basico+propinas;
        return salary;
    }
    public void menu(){
        for(String menu: mesero){
            System.out.println(menu);
        }
    }
   // public void pedidos(){
     //   System.out.println("Cantidad de platos: ");
    //}
}
