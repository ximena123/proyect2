/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2;
import java.io.*;
import java.util.*;

/**
 *
 * @author Ximena Puchaicela
 */
public class Menu extends  Recipe {
    static ArrayList<String>cocinero= new  ArrayList<String>();
    static ArrayList<String>mesero= new  ArrayList<String>();
    // verifica si un archivo no existe lo crea //
    public static boolean file(File ArchivoTXT) {
        try {
            if (ArchivoTXT.exists()) {
                System.out.println("RECETARIO.");
                return true;

            } else {
                ArchivoTXT.createNewFile();
                System.out.println("RECETARIO");
                return false;
            }
        } catch (IOException e) {
            System.out.println("No se encontro recetario");
        }
        return false;
    }
    // verifica si el existe un archivo con recetas dentro//
    public boolean verify(File ArchivoTXT){
        if(ArchivoTXT.exists())
            //System.ou,t.println("RECETARIO ");
            return true;
        else     
            return false;
           // System.out.println("No existen recetas disponibles");
    }
    // metodo para poder agregar receta en el archivo de texto//
    public void add_recipe(File ArchivoTXT, Recipe objRecipe) {
        String separator = "/";
        Scanner teclado = new Scanner(System.in);
        try {
            BufferedWriter Writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ArchivoTXT, true)));
           // manejar el file en modo escritura/true que lo siga escribiendo al final del file  
//            System.out.println("Ingrese el codigo de la receta: ");llamada.setCode(teclado.nextLine());
//            System.out.print("Ingrese el nombre de la receta: ");llamada.setName(teclado.nextLine());
//            System.out.print("Ingrese los ingredientes: ");llamada.setIngredients(teclado.nextLine());
//            System.out.println("Ingrese el procedimiento: ");llamada.setProcess(teclado.nextLine());
//            System.out.println("Ingrese el precio: ");llamada.setPrice(teclado.nextInt());
//            llamada.getSize();
            // mediante el objeto escribir se  invoca a un metodo write para poder escribir en el file
            System.out.println(objRecipe.toString());
            Writer.write(objRecipe.getSize() + separator +objRecipe.getCode()+separator+ objRecipe.getName() + separator + objRecipe.getIngredients() + separator + objRecipe.getProcess() + separator+objRecipe.getPrice()+separator);
            Writer.close();
            System.out.println("Receta agregada correctamente");
            //Exception palabra que engloba las excepciones
        } catch (Exception e) {
            System.out.println("No se pudo agregar la receta"+e);
        }
    }
    // busca la receta segun el nombre de la receta//
    public void  search(File ArchivoTXT) {
        String name= "null";
        Scanner teclado = new Scanner(System.in);
        String search, read;
        int cont =0;
        try {
            // crea un nuevo objeto de la clase(instanciar)
            BufferedReader identify = new BufferedReader(new FileReader(ArchivoTXT));
            System.out.println("Ingrese el nombre de la receta: ");search = teclado.next(); 
            // accede a una propiedad del objeto
            //read line lee una linea hasta que se encuentre con un caracter
            read = identify.readLine();
            //cuando termina de recorrer el file se vuelve nulo
            while (read != null) {
                StringTokenizer indice = new StringTokenizer(read, "/");
                //el metodo contains devuelve un valor true si encuentra la palabra buscada
                if (read.contains(search)) {
                    while (name != search) {
                        //trim metodo para eliminar los espacios al principio y al final de las cadenas
                        String record_size = indice.nextToken().trim();
                        String code = indice.nextToken().trim();
                        name = indice.nextToken().trim();
                        String ingredients = indice.nextToken().trim();
                        String process = indice.nextToken().trim();
                        String preci =indice.nextToken().trim() ;
                        int numInt = Integer.parseInt(preci);
                        if (name.equals(search)) {
                            cocinero.add("Nombre: "+name+"\nIngredientes: "+ingredients+"\nProcedimiento: "+process);
                            mesero.add(code+" "+name+"  "+numInt); 
                            cont++;
                        }
                    }      
                }
               
                identify.close();
            } 

        } catch (Exception e) {
            System.out.println("");
        }
         if(cont == 0)
                    System.out.println("No existen recetas con el ingrediente ingresado");
        
    }

    
}
